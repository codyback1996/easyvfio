# ChodeTode's Single GPU Passthrough Scripts

    These scripts can be used to get gpu passthrough setup on a single gpu.

## Prerequisites
In order to get this working there's a few things you must check first

1. A motherboard that supports IOMMU. Most modern motherboards should support this, but make sure to check.

2. A cpu that supports hardware virtualization.

3. An nvidia gpu (A method for amd gpus is being worked on, but for now, only nvidia will work).

And that's it! Now you can move on to setting up the script.

## Setup

to be added later.

## known issues

1. When running the script on Ubuntu 18.04, the VM may fail to boot leaving the user stuck on a black screen till a manual system reboot is done. The odds of this happening are about 5 out of 10 based on testing with Ubuntu. It's assumed that this issue is caused by certain processes being unable to close, which results in the Display manager not closing and the gpu not being unbind for the VM. It's unknown if other flavors of Ubuntu are being affected by this issue.

2. Windows may stop being responsive after falling asleep. This issue has only come up a few times and its cause is unknown. A workaround for this issue is to disable sleep and hibernation modes for windows.

3. After very long uses, system audio may become very choppy. This might be an issue with QEMU. A workaround is to switch your default audio device around in windows.

4. Windows 10 1803 is known to have very bad performance. This is due to the included Specter patch.

## TODO
- [ ] Improve the readme file.
- [ ] Write documentation on how to set it up.
- [ ] Remove unnecessary sleeps from the windows.sh script to improve the boot and shut down speed.
- [ ] Fix issues with Ubuntu 18.04.
- [ ] Make it easier to configure.
- [ ] Write a script to automatically detect and apply IOMMU groups to the config file.
- [ ] Completely eliminate the need for a GPU vbios file, or make the process to get one easier.
- [ ] Write a script to automatically detect user system info and apply it to the config file. 
- [ ] Write a easy to use setup program in python to make the install process way easier.
- [ ] Look into finding a way to extract amd gpu bios.
- [ ] Verify amd cpu and gpu support.
- [ ] Add scripts for macOS support.